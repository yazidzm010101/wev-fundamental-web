# IMAGE

<a href="https://www.w3schools.com/html/html_images.asp">
    <img src="https://upload.wikimedia.org/wikipedia/commons/d/de/Simpleicons_Interface_link-symbol.svg"
        height=16 color=blue/>
    w3schools
</a>

Dalam dokumen html kita dapat menampilkan gambar dari filesystem(lokal) ataupun dari website lain(online)

- ## SECARA LOKAL
    ```html
    <img src="./assets/img/cilung.jpg" alt="cilung">
    ```
    > <img src="./assets/img/cilung.jpg" alt="cilung" width=300>

- ## SECARA ONLINE
    ```html
    <img src="https://i2.wp.com/greatnesia.id/wp-content/uploads/2019/11/cilung.jpg?resize=1024%2C576&ssl=1" alt="cilung">
    ```
    > <img src="https://i2.wp.com/greatnesia.id/wp-content/uploads/2019/11/cilung.jpg?resize=1024%2C576&ssl=1" alt="cilung" width=300>

- ## MENGATUR UKURAN

    Kita juga dapat mengatur ukuran dari gambar melalui atribut ataupun style

    ```html
    <img src="./assets/img/cilung.jpg" alt="cilung" width=300>
    <img src="./assets/img/cilung.jpg" alt="cilung" style="width: 200;">
    <img src="./assets/img/cilung.jpg" alt="cilung" style="width: 100;">
    ```
    > <img src="./assets/img/cilung.jpg" alt="cilung" width=300/>
    > <img src="./assets/img/cilung.jpg" alt="cilung" width=200/>
    > <img src="./assets/img/cilung.jpg" alt="cilung" width=100/>


# LIST

<a href="https://www.w3schools.com/HTML/html_lists.asp">
    <img src="https://upload.wikimedia.org/wikipedia/commons/d/de/Simpleicons_Interface_link-symbol.svg"
        height=16 color=blue/>
    w3schools
</a>

Untuk membuat list(daftar) bisa menggunakan list yang berurutan(ordered list ```ol```) ataupun
tidak berurutan(unordered list ```ul```). Kedua jenis list digunakan sesuai dengan konteks dari daftar
yang ditampilkan.


- ## Unordered List

    <a href="https://www.w3schools.com/HTML/html_lists_unordered.asp">
    <img src="https://upload.wikimedia.org/wikipedia/commons/d/de/Simpleicons_Interface_link-symbol.svg" height=16 color=blue/>
    w3schools
    </a>

    ```html
    <ul style="list-style-type:disc;">
        <li>Poin satu</li>
        <li>Poin dua</li>
        <li>Poin tiga</li>
        <li>Poin empat</li>
    </ul>
    ```

    > <ul>
    >     <li>Poin satu</li>
    >     <li>Poin dua</li>
    >     <li>Poin tiga</li>
    >     <li>Poin empat</li>
    > </ul>


- ## Ordered List

    <a href="https://www.w3schools.com/HTML/html_lists_ordered.asp">
    <img src="https://upload.wikimedia.org/wikipedia/commons/d/de/Simpleicons_Interface_link-symbol.svg" height=16 color=blue/>
    w3schools
    </a>

    ```html
    <p>Cara masak cilung</p>
    <ol type="i">
        <li>Pertama siapkan uang</li>
        <li>Datang ke tukang cilung</li>
        <li>Beli sekitar 1-3 stick sesuai nafsu anda</li>
        <li>Jangan lupa pake royco</li>
        <li>Enjoy the food</li>
    </ol>
    ```

    > Cara masak cilung
    > <ol type="i">
    >     <li>Pertama siapkan uang</li>
    >     <li>Datang ke tukang cilung</li>
    >     <li>Beli sekitar 1-3 stick sesuai nafsu anda</li>
    >     <li>Jangan lupa pake royco</li>
    >     <li>Enjoy the food</li>
    > </ol>
