# DASAR HTML

Repository ini berisi beberapa tag yang merupakan dasar dari html. Pada setiap folder sudah disediakan contoh source code disertai dokumentasinya.

**Untuk mempelajari jauh lebih dalam HTML silahkan akses langsung tutorial dari w3schools [ini](https://www.w3schools.com/HTML/default.asp) atau pelajari video-video keren ini**

- [The Net Ninja](https://www.youtube.com/watch?v=OwC4xNWihoM&list=PL4cUxeGkcC9ibZ2TSBaGGNrgh4ZgYE6Cc)
- [Unpas](https://www.youtube.com/watch?v=NBZ9Ro6UKV8&list=PLFIM0718LjIVuONHysfOK0ZtiqUWvrx4F)
