# HEAD

<a href="https://www.w3schools.com/tags/tag_head.asp">
    <img src="https://upload.wikimedia.org/wikipedia/commons/d/de/Simpleicons_Interface_link-symbol.svg"
        height=16 color=blue/>
    w3schools
</a>

Layaknya semua jenis file yang kita kenal pasti ada informasi tambahan
layaknya file foto yang punya informasi seperti
- kapan foto diambil
- camera yang digunakan
- resolusi, dll

Begitu juga dokumen html yang pada tag ```<head>``` yang 
memuat informasi tambahan dari web page berupa
- metadata seperti
    - deskripsi
    - keyword(pengaruhnya di search engine)
- styling(css) dan script(javascript)
- judul pada tab browser, dll

contoh:
```html
<head>        
    <meta charset="utf-8" />
    <meta name="keywords" content="cilung, aci digulung, cilung murah sedepok, cilung manteb sedepok"> 
    <meta name="descriptions" content="Kita jualan cilung termurah terenak di Depok">
    <title>Website kang cilung</title>         
</head>
```

# BODY

<a href="https://www.w3schools.com/tags/tag_body.asp">
    <img src="https://upload.wikimedia.org/wikipedia/commons/d/de/Simpleicons_Interface_link-symbol.svg"
        height=16 color=blue/>
    w3schools
</a>

memuat konten yang ditampilin di layar browser seperti
- Heading
- Paragraph
- Gambar, dll

contoh:
```html
<body>
    <h1>Hi semua!</h1>
    <p>ini adalah website pertama saya, kita jualan cilung loh! &#128523</p>         
</body>
```