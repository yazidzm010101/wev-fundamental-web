# FIRST OF ALL

Layaknya kita pakai word processing seperti Ms Word,
html juga memiliki tag-tag untuk memenuhi performatan dasar
layaknya dokumen word!


## HEADING

<a href="https://www.w3schools.com/html/html_headings.asp">
    <img src="https://upload.wikimedia.org/wikipedia/commons/d/de/Simpleicons_Interface_link-symbol.svg"
        height=16 color=blue/>
    w3schools
</a>

Ini serupa dengan Heading 1 sampai 6 yang ada pada Ms Word,
Dari ```h1``` sampai ```h6``` maka semakin kecil ukuran teksnya,
dan semakin berkurang "level of important" yang dimiliki heading.
contoh :

```html
<h1>Ini tag H1</h1>
<h2>Ini tag H2</h2>
<h3>Ini tag H3</h3>
<h4>Ini tag H4</h4>
<h5>Ini tag H5</h5>
<h6>Ini tag H6</h6>
```

> <h1>Ini tag H1</h1>
> <h2>Ini tag H2</h2>
> <h3>Ini tag H3</h3>
> <h4>Ini tag H4</h4>
> <h5>Ini tag H5</h5>
> <h6>Ini tag H6</h6>

## PARAGRAPH

<a href="https://www.w3schools.com/html/html_paragraphs.asp">
    <img src="https://upload.wikimedia.org/wikipedia/commons/d/de/Simpleicons_Interface_link-symbol.svg"
        height=16 color=blue/>
    w3schools
</a>

Layaknya paragraph yang ada di word, html juga terdapat
tag paragraph buat nampilin kalimat!

```html
<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. 
    Neque numquam veniam voluptatem eveniet quasi error eos 
    ipsam repellendus quia cupiditate sequi aliquid accusantium 
    excepturi, esse quos suscipit tenetur eius praesentium!
</p>
```

><p>Lorem ipsum dolor sit amet consectetur adipisicing elit. 
>    Neque numquam veniam voluptatem eveniet quasi error eos 
>    ipsam repellendus quia cupiditate sequi aliquid accusantium 
>    excepturi, esse quos suscipit tenetur eius praesentium!</p>

## FORMATTING

Kita juga bisa memformat teks layaknya di word!

- Membuat teks menjadi berukuran kecil!

    ```html    
    <h4>
        BESAR<small>kecil</small>
    </h4>
    ```    

    > <h4>BESAR<small>kecil</small></h4>

- Membuat teks menjadi tebal, miring, dicoret!

    ```html
    <p>
        Aku <b>tebal</b> <i>miring</i> <s>dicoret!</s>
    </p>
    ```    

    > <p>Aku <b>tebal</b> <i>miring</i> <s>dicoret!</s></p>

- Baris baru, Superscript, Subscript, Marker!    

    ```html
        <p>
            2 <sup>n</sup> 
            <br/>
            U <sub>n</sup>
            <br/>
            I hate <mark>MATH</mark> !
        </p>
    ```    

    > <p>
    >     2 <sup>n</sup> 
    >     <br/>
    >     U <sub>n</sup>
    >     <br/>
    >     I hate <mark>MATH</mark> !
    > </p>
