# ANCHOR TAG 
<a href="https://www.w3schools.com/tags/tag_a.asp">
    <img src="https://upload.wikimedia.org/wikipedia/commons/d/de/Simpleicons_Interface_link-symbol.svg"
        height=16 color=blue/>
    w3schools
</a>

Tag ini digunakan untuk melakukan hyperlink pada section dokumen tertentu,
atau berpindah ke halaman/website lain.

- Untuk berpindah ke section tertentu, kita perlu memberikan atribut id pada section yang kita tuju.<br/>
  Value dari atribut href diawali dengan simbol '#'
    ```html
        <a href="#contact-us">Don't hesitate to contact us!</a>
        <!-- Ceritanya ada banyak konten disini -->
        <!-- Ceritanya ada banyak konten disini -->
        <!-- Ceritanya ada banyak konten disini -->
        <p id="contact-us">Yeay, you found me!</p>
    ```

- Untuk berpindah ke halaman lain, kita hanya perlu memasukkan url yang kita inginkan pada atribut href
    ```html
        <a href="https://www.w3schools.com/tags/tag_a.asp">Find me on w3schools!</a>        
    ```